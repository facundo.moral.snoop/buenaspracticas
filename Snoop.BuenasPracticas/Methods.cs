﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoop.BuenasPracticas
{
    public static class Methods
    {
        #region 
        public static int GetPersons() { return 0; }
        public static object GetPersonId() { return null; }
        public static string ValidateInput() { return ""; }
        #endregion

        #region Example 7
        public static bool Example7(int value1, int value2)
        {
            #region 
            string username = "";
            int codeId = 0;
            List<string> otherData = new List<string>();
            int areaId = 0;
            string password = "";
            long price = 0;
            long kg = 0;
            string dogname = "";
            string ip = "";
            string smtp = "";
            #endregion

            if (value1 > 10 && value1 < 20) { return false; }
            if (value2 > 40 && value2 < 60) { return false; }

            // more operations with value1 and value2
            return true;
        }

        private static long GetKg()
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
