﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoop.BuenasPracticas.Models
{
    public class Payment
    {
        public PaymentStatus PaymentStatus { get; set; } = PaymentStatus.PAID;
    }

    public enum PaymentStatus
    {
        PAID,
        CANCEL
    }
}
