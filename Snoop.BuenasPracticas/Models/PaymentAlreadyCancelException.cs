﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoop.BuenasPracticas.Models
{
    public class PaymentAlreadyCancelException : Exception
    {
        public PaymentAlreadyCancelException(string message) : base(message) { }
    }
}
