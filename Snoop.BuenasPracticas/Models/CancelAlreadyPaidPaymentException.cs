﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Snoop.BuenasPracticas.Models
{
    public class CancelAlreadyPaidPaymentException : Exception
    {
        public CancelAlreadyPaidPaymentException(string message) : base(message)
        { 
        
        }
    }
}
