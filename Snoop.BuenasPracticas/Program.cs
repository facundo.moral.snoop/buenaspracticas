﻿using Snoop.BuenasPracticas.Models;
using System;
using System.Collections.Generic;

namespace Snoop.BuenasPracticas
{
    class Program
    {
        #region Helpers
        static DateTime startTime;
        static int zero = 0;
        static void StartTimer()
        {
            startTime = DateTime.Now;
        }
        static void GetElapsedTime()
        {
            var finalTime = DateTime.Now;
            Console.WriteLine((finalTime - startTime).TotalMilliseconds + " ms");
        }
        #endregion


        static void Main(string[] args)
        {
            #region Example 1
            StartTimer();
            Exceptions.Example1_ThrowException(zero);
            Console.WriteLine("Throw exception: ");
            GetElapsedTime();

            StartTimer();
            Exceptions.Example1_ValidateNumber(zero);
            Console.WriteLine("Validate number: ");
            GetElapsedTime();
            #endregion

            #region Example 2
            Exceptions.Example2_CancelPayment_OneValidation(new Payment());
            #region
            try
            {
                Exceptions.Example2_CancelPayment_OneValidation(new Payment());
            }
            catch (CancelAlreadyPaidPaymentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            #endregion

            Exceptions.Example2_CancelPayment_TwoValidation(new Payment());
            #region
            try
            {
                Exceptions.Example2_CancelPayment_TwoValidation(new Payment());
            }
            catch (CancelAlreadyPaidPaymentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (PaymentAlreadyCancelException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                // ??
            }
            #endregion

            #region
            var resultOperation = Exceptions.Example2_CancelPayment_TwoValidation(new Payment());
            if (resultOperation)
            {
                // ok
            }
            else
            { 
                // error
            }
            #endregion
            #endregion

            #region Example 3
            Exceptions.Example3_LongTryStatement();
            #endregion

            #region Example 4
            Exceptions.Example4_VoidLogMethod();
            #endregion

            #region Example 5
            Exceptions.Example5_StringReturn();
            Exceptions.Example5_IntReturn();
            Exceptions.Example5_ListReturn();
            #endregion

            #region Example 6
            object person = Methods.GetPersonId();
            string check = Methods.ValidateInput();
            int persons = Methods.GetPersons();
            const int age_limit = 10;
            #endregion

            #region Example 7
            Methods.Example7(10, 20);
            #endregion
            Console.ReadKey();
        }


        #region Helpers
        private static void ObtenerName()
        {
            throw new NotImplementedException();
        }
        private static List<string> GetEdad()
        {
            return new List<string>();
        }
        private static object ValidarFecha()
        {
            return null;
        }
        #endregion
    }
}
