﻿using Snoop.BuenasPracticas.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Snoop.BuenasPracticas
{
    public class Exceptions
    {
        #region Example 1
        public static (bool, int) Example1_ThrowException(int number)
        {
            try
            {
                return (true, 10 / number);
            }
            catch (DivideByZeroException e)
            {
                return (false, 0);
            }
        }

        public static (bool, int) Example1_ValidateNumber(int number)
        {
            return number == 0 ?
                (false, 0) :
                (true, 10 / number);
        }

        #endregion

        #region Example 2
        public static bool Example2_CancelPayment_OneValidation(Payment payment)
        {
            if (payment.PaymentStatus == PaymentStatus.PAID)
            {
                throw new CancelAlreadyPaidPaymentException("Cannot cancel already paid payment");
            }
            // operation
            return true;
        }

        public static bool Example2_CancelPayment_TwoValidation(Payment payment)
        {
            if (payment.PaymentStatus == PaymentStatus.PAID)
            {
                throw new CancelAlreadyPaidPaymentException("Cannot cancel already paid payment");
            }
            if (payment.PaymentStatus == PaymentStatus.CANCEL)
            {
                throw new PaymentAlreadyCancelException("Payment already cancel");
            }
            return true;
        }

        #endregion

        #region Example 3
        public static void Example3_LongTryStatement()
        {
            try
            {
                #region 1000 lines code












































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































































                #endregion
            }
            catch (Exception ex)
            {
                // throw new Exception(ex.Message);
                // =>
                throw ex;
                // =>
                throw;
            }
        }
        #endregion

        #region Example 4
        public static void Example4_VoidLogMethod()
        {
            try
            {
                // operation
            }
            catch (Exception e)
            {
                Log(e);
            }
        }
        #endregion

        #region Example 5
        public static string Example5_StringReturn()
        {
            try
            {
                var nombre = GetNombre();
                return nombre;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        public static int Example5_IntReturn()
        {
            try
            {
                var edad = GetEdad();
                return edad;
            }
            catch (Exception ex)
            {
                return ex.HResult;
            }
        }
        public static List<string> Example5_ListReturn()
        {
            try
            {
                return GetNames();
            }
            catch (Exception ex)
            {
                return new List<string>();
            }
        }
        #endregion


        #region Helpers
        private static void Log(Exception e)
        {
            throw new NotImplementedException();
        }

        private static string GetNombre()
        {
            return "";
        }
        private static int GetEdad()
        {
            return 0;
        }
        private static List<string> GetNames() { return new List<string>(); }
        #endregion
    }
}
